//[SECTION] Introduction to JSON (JavaScript Object Notation)

//JSON -> is a 'Data Representation Format' similar to XML and YAML

//Uses of JSON data format:

	//=> Commonly used for API and Configuration.
	//=> JSON is also used in serealizing different data types into 'bytes'. 

    //What is Serealizing?
      //=> is the process of converting data into a series of 'bytes' for easier transmission/transfer of information

      //a 'byte' => is a unit of data that is eight binary digits (1 and 0) that is used to represent a character(letters, numbers, typographic symbols). 

    //Benefits => once a piece of data/information has been serialize they become 'lightweight', it becomes a lot easier to transfer or trasmit over a network or connection. 

//[SECTION] Structure of JSON format

  //=> JSON data is similar to the structure of a JS Object.
  //=> JS Objects are NOT to be confused with JSON. 
  //SYNTAX:
    // {
    // 	"propertyA": "valueA",
    // 	"propertyB": "valueB"
    // }

    //JSON uses double quotes ("") for its property names.

    //example: 
    // {
    // 	"city": "Quezon City",
    // 	"province": "Metro Manila",
    // 	"country": "Philippines"
    // }

//[SECTION] JSON types
   //JSON will accept the following values:
   //1. Strings => "Hello", "Hello World"
   //2. Number => 10, 1.5, -30, 1.2e10
   //3. Boolean => True or False
   //4. null => empty/null
   //5. Array => []
   //6. Object => { key: value }
